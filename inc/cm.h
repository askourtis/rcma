#pragma once

#include "definitions.h"

typedef double volatile DataType;
typedef Index  volatile IndexType;
typedef Size   volatile SizeType;

//Define Generics for type index type
#define TYPE DataType
#include "generics.h"
#undef TYPE

//Define Generics for type data type
#define TYPE IndexType
#include "generics.h"
#undef TYPE


/**
 * @brief Performs the Cuthill-Mckee algorithm on a given sparse matrix
 * 
 * 
 * @param I The row indicies
 * @param J The column indicies
 * @param V The entries
 * @param N The number of rows or columns
 * @param L The number of total entries
 * 
 * @return An array list with the permutations computed by the algorithm
 */
ArrayList(IndexType)* cms(IndexType const* I, IndexType const* J, DataType const* V, SizeType N, SizeType L);

/**
 * @brief Performs the Reverse-Cuthill-Mckee algorithm on a given sparse matrix
 * 
 * 
 * @param I The row indicies
 * @param J The column indicies
 * @param V The entries
 * @param N The number of rows or columns
 * @param L The number of total entries
 * 
 * @return An array list with the permutations computed by the algorithm
 */
ArrayList(IndexType)* rcms(IndexType const* I, IndexType const* J, DataType const* V, SizeType N, SizeType L);
