//No guard
#ifdef TYPE

#include "definitions.h"
#include <memory.h>
#include <stdlib.h>

//Needed Macros for name concatinations
#define CONCAT_(FIRST, SECOND) FIRST ## SECOND
#define CONCAT(FIRST, SECOND)  CONCAT_(FIRST, SECOND)
#define TYPED_NAME(NAME, TYPE) CONCAT(NAME, CONCAT(_TYPED_TO_, TYPE))

//Section: Data Structures
//Section: Matrix
//+================+
//+                +
//+ GENERIC MATRIX +
//+                +
//+================+

/**
 * @brief A generic matrix data structure
 *
 * @note The matrix is RowMajor
 */
#define Matrix(TYPE)    TYPED_NAME(Matrix, TYPE)
#define GenericMatrix   Matrix(TYPE)

typedef struct {
    Size volatile const _cols, _rows;
    TYPE _data[];
} GenericMatrix;

//Name Definitions
/**
 * @brief Initializes a block of memory as a generic matrix
 *
 * @param addr  The block off memory to initialize
 * @param cols  The number of columns of the matrix
 * @param rows  The number of rows of the matrix
 *
 * @return The same address casted as GenericMatrix
 */
#define matrix_init(TYPE) TYPED_NAME(matrix_init, TYPE)

/**
 * @brief Allocates and initializes a block of memory as a generic matrix
 *
 * @param cols  The number of columns of the matrix
 * @param rows  The number of rows of the matrix
 *
 * @return The adress of the new matrix or null if the allocation failed
 */
#define matrix_construct(TYPE) TYPED_NAME(matrix_construct, TYPE)

/**
 * @brief Allocates enough space and copies into that space the given matrix
 *
 * @param mat  The adress of the matrix to copy
 *
 * @return The adress of the new matrix
 */
#define matrix_copy(TYPE) TYPED_NAME(matrix_copy, TYPE)

/**
 * @brief Copies a matrix to the destination adress
 *
 * @param mat   The adress of the matrix to copy
 * @param dest  The destination adress
 *
 * @return The destination adress casted as GenericMatrix or null if the allocation fails
 */
#define matrix_copy_to(TYPE) TYPED_NAME(matrix_copy_to, TYPE)

/**
 * @brief Destroys a matrix
 *
 * @param mat  The matrix to destroy
 */
#define matrix_destroy(TYPE) TYPED_NAME(matrix_destroy, TYPE)

/**
 * @brief Gets the size of a matrix
 *
 * @param mat  The matrix to get the size from
 *
 * @return the size of the matrix
 */
#define matrix_size(TYPE) TYPED_NAME(matrix_size, TYPE)

/**
 * @brief Gets the number of columns of a matrix
 *
 * @param mat  The matrixx to get the columns from
 *
 * @return the number of columns of the matrix
 */
#define matrix_cols(TYPE) TYPED_NAME(matrix_cols, TYPE)

/**
 * @brief Gets the number of rows of a matrix
 *
 * @param mat  The matrix to get the rows from
 *
 * @return the number of rows of the matrix
 */
#define matrix_rows(TYPE) TYPED_NAME(matrix_rows, TYPE)

/**
 * @brief Gets the adress of a matrix's data
 *
 * @param mat  The matrix to get the data from
 *
 * @return the adress to the data
 */
#define matrix_const_data(TYPE) TYPED_NAME(matrix_const_data, TYPE)

/**
 * @brief Gets the adress of a matrix's data
 *
 * @param mat  The matrix to get the data from
 *
 * @return the adress to the data
 */
#define matrix_data(TYPE) TYPED_NAME(matrix_data, TYPE)

/**
 * @brief Gets the value at the given indicies of a matrix
 *
 * @param mat  The matrix
 * @param i    The row index
 * @param j    The column index
 *
 * @return the selected value
 */
#define matrix_at(TYPE) TYPED_NAME(matrix_at, TYPE)

/**
 * @brief Sets the value at the given indicies of a matrix
 *
 * @param mat    The matrix
 * @param i      The row index
 * @param j      The column index
 * @param value  The new value
 */
#define matrix_set(TYPE) TYPED_NAME(matrix_set, TYPE)

//Generic Name Definitions
#define generic_matrix_init         matrix_init         (TYPE)
#define generic_matrix_construct    matrix_construct    (TYPE)
#define generic_matrix_copy         matrix_copy         (TYPE)
#define generic_matrix_copy_to      matrix_copy_to      (TYPE)
#define generic_matrix_destroy      matrix_destroy      (TYPE)
#define generic_matrix_size         matrix_size         (TYPE)
#define generic_matrix_cols         matrix_cols         (TYPE)
#define generic_matrix_rows         matrix_rows         (TYPE)
#define generic_matrix_const_data   matrix_const_data   (TYPE)
#define generic_matrix_data         matrix_data         (TYPE)
#define generic_matrix_at           matrix_at           (TYPE)
#define generic_matrix_set          matrix_set          (TYPE)

//Generic Function Prototypes
static GenericMatrix*   generic_matrix_init         (void* addr, Size cols, Size rows)                                              ;
static GenericMatrix*   generic_matrix_construct    (Size cols, Size rows)                                                          ;
static GenericMatrix*   generic_matrix_copy         (GenericMatrix const* mat)                                                      ;
static GenericMatrix*   generic_matrix_copy_to      (GenericMatrix const* mat, void* dest)                                          ;
static void             generic_matrix_destroy      (GenericMatrix      * mat)                                                      ;
static Size             generic_matrix_size         (GenericMatrix const* mat)                                  __attribute_pure__  ;
static Size             generic_matrix_cols         (GenericMatrix const* mat)                                  __attribute_pure__  ;
static Size             generic_matrix_rows         (GenericMatrix const* mat)                                  __attribute_pure__  ;
static TYPE const*      generic_matrix_const_data   (GenericMatrix const* mat)                                  __attribute_const__ ;
static TYPE      *      generic_matrix_data         (GenericMatrix      * mat)                                  __attribute_const__ ;
static TYPE             generic_matrix_at           (GenericMatrix const* mat, Index i, Index j)                __attribute_pure__  ;
static void             generic_matrix_set          (GenericMatrix      * mat, Index i, Index j, TYPE value)                        ;


//Section: Vector
//+================+
//+                +
//+ GENERIC VECTOR +
//+                +
//+================+

/**
 * @brief A generic vector data structure
 */
#define Vector(TYPE)  TYPED_NAME(Vector, TYPE)
#define GenericVector Vector(TYPE)

typedef GenericMatrix GenericVector;

//Name definitions
/**
 * @brief Initializes a block of memory as a generic vector
 *
 * @param addr  The block off memory to initialize
 * @param size  The size of the vector
 *
 * @return The same address casted as GenericVector
 */
#define vector_init(TYPE) TYPED_NAME(vector_init, TYPE)

/**
 * @brief Allocates and initializes a block of memory as a generic vector
 *
 * @param size  The size of the vector
 *
 * @return The adress of the new vector or null if the allocation failed
 */
#define vector_construct(TYPE) TYPED_NAME(vector_construct, TYPE)

/**
 * @brief Allocates enough space and copies into that space the given vector
 *
 * @param vec  The adress of the vector to copy
 *
 * @return The adress of the new vector or null if the allocation fails
 */
#define vector_copy(TYPE) TYPED_NAME(vector_copy, TYPE)

/**
 * @brief Copies a vector to the destination adress
 *
 * @param vec   The adress of the vector to copy
 * @param dest  The destination adress
 *
 * @return The destination adress casted as GenericVector
 */
#define vector_copy_to(TYPE) TYPED_NAME(vector_copy_to, TYPE)

/**
 * @brief Destroys a vector
 *
 * @param vec  The vector to destroy
 */
#define vector_destroy(TYPE) TYPED_NAME(vector_destroy, TYPE)

/**
 * @brief Gets the size of a vector
 *
 * @param vec  The vector to get the size from
 *
 * @return the size of the vector
 */
#define vector_size(TYPE) TYPED_NAME(vector_size, TYPE)

/**
 * @brief Gets the adress of a vector's data
 *
 * @param vec  The vector to get the data from
 *
 * @return the adress to the data
 */
#define vector_data(TYPE) TYPED_NAME(vector_data, TYPE)

/**
 * @brief Gets the adress of a vector's data
 *
 * @param vec  The vector to get the data from
 *
 * @return the adress to the data
 */
#define vector_const_data(TYPE) TYPED_NAME(vector_const_data, TYPE)

/**
 * @brief Gets the value at the given index of a vector
 *
 * @param vec  The vector
 * @param i    The index
 *
 * @return the selected value
 */
#define vector_at(TYPE) TYPED_NAME(vector_at, TYPE)

/**
 * @brief Sets the value at the given index of a vector
 *
 * @param vec    The vector
 * @param i      The index
 * @param value  The new value
 */
#define vector_set(TYPE) TYPED_NAME(vector_set, TYPE)

//Generic Name definitions
#define generic_vector_init        vector_init          (TYPE)
#define generic_vector_construct   vector_construct     (TYPE)
#define generic_vector_copy        vector_copy          (TYPE)
#define generic_vector_copy_to     vector_copy_to       (TYPE)
#define generic_vector_destroy     vector_destroy       (TYPE)
#define generic_vector_size        vector_size          (TYPE)
#define generic_vector_data        vector_data          (TYPE)
#define generic_vector_const_data  vector_const_data    (TYPE)
#define generic_vector_at          vector_at            (TYPE)
#define generic_vector_set         vector_set           (TYPE)

//Generic Function Prototypes
static GenericVector* generic_vector_init      (void* addr, Size size)                                                      ;
static GenericVector* generic_vector_construct (Size size)                                                                  ;
static GenericVector* generic_vector_copy      (GenericVector const* vec)                                                   ;
static GenericVector* generic_vector_copy_to   (GenericVector const* vec, void* dest)                                       ;
static void           generic_vector_destroy   (GenericVector      * vec)                                                   ;
static Size           generic_vector_size      (GenericVector const* vec)                                __attribute_pure__ ;
static TYPE const*    generic_vector_const_data(GenericVector const* vec)                                __attribute_const__;
static TYPE*          generic_vector_data      (GenericVector      * vec)                                __attribute_const__;
static TYPE           generic_vector_at        (GenericVector const* vec, Index const i)                 __attribute_pure__ ;
static void           generic_vector_set       (GenericVector      * vec, Index const i, TYPE value)                        ;


//Section: Vector
//+====================+
//+                    +
//+ GENERIC ARRAY LIST +
//+                    +
//+====================+

/**
 * @brief A generic arraylist data structure
 */
#define ArrayList(TYPE)  TYPED_NAME(ArrayList, TYPE)
#define GenericArrayList ArrayList(TYPE)

typedef struct {
    Size volatile   _size;
    GenericVector   _data;
} GenericArrayList;

//Name definitions
/**
 * @brief Initializes a block of memory as a generic arraylist
 *
 * @param addr      The block off memory to initialize
 * @param capacity  The capacity of the arraylist
 *
 * @return The same address casted as GenericArrayList
 */
#define array_list_init(TYPE) TYPED_NAME(array_list_init, TYPE)

/**
 * @brief Allocates and initializes a block of memory as a generic arraylist
 *
 * @param capacity  The capacity of the arraylist
 *
 * @return The adress of the new arraylist or null if the allocation failed
 */
#define array_list_construct(TYPE) TYPED_NAME(array_list_construct, TYPE)

/**
 * @brief Allocates enough space and copies into that space the given arraylist
 *
 * @param array  The adress of the arraylist to copy
 *
 * @return The adress of the new arraylist or null off the allocation fails
 */
#define array_list_copy(TYPE) TYPED_NAME(array_list_copy, TYPE)

/**
 * @brief Copies an arraylist to the destination adress
 *
 * @param array  The adress of the arraylist to copy
 * @param dest   The destination adress
 *
 * @return The destination adress casted as GenericArrayList
 */
#define array_list_copy_to(TYPE) TYPED_NAME(array_list_copy_to, TYPE)

/**
 * @brief Destroys an arraylist
 *
 * @param array  The arraylist to destroy
 */
#define array_list_destroy(TYPE)     TYPED_NAME(array_list_destroy,    TYPE)

/**
 * @brief Pushes a value at the end of an array list increasing its size but not its capacity
 *
 * @param array  The arraylist to push into
 * @param value  The value to push
 *
 * @return True if the push was successful, false otherwise
 */
#define array_list_push(TYPE) TYPED_NAME(array_list_push, TYPE)

/**
 * @brief Deletes an element at the given index and reorders the data to maintain a contineous block of memory
 *
 * @param array  The arraylist to remove from
 * @param index  The index of the value to delete
 */
#define array_list_erase(TYPE) TYPED_NAME(array_list_erase, TYPE)

/**
 * @brief Gets the value at the given index of an arraylist
 *
 * @param array  The arraylist
 * @param i      The index
 *
 * @return the selected value
 */
#define array_list_at(TYPE) TYPED_NAME(array_list_at, TYPE)

/**
 * @brief Sets the value at the given index of an arraylist
 *
 * @param array  The arraylist
 * @param i      The index
 * @param value  The new value
 */
#define array_list_set(TYPE) TYPED_NAME(array_list_set, TYPE)

/**
 * @brief Gets the adress of a arraylist's data
 *
 * @param array  The arraylist to get the data from
 *
 * @return the adress to the data
 */
#define array_list_data(TYPE) TYPED_NAME(array_list_data, TYPE)

/**
 * @brief Gets the adress of a arraylist's data
 *
 * @param array  The arraylist to get the data from
 *
 * @return the adress to the data
 */
#define array_list_const_data(TYPE)  TYPED_NAME(array_list_const_data, TYPE)

/**
 * @brief Gets the size of an arraylist
 *
 * @param array  The arraylist to get the size from
 *
 * @return the size of the arraylist
 */
#define array_list_size(TYPE) TYPED_NAME(array_list_size, TYPE)

/**
 * @brief Gets the capacity of an arraylist
 *
 * @param array  The arraylist to get the capacity from
 *
 * @return the capacity of the arraylist
 */
#define array_list_capacity(TYPE) TYPED_NAME(array_list_capacity, TYPE)

//Generic Name definitions
#define generic_array_list_init                 array_list_init          (TYPE)
#define generic_array_list_construct            array_list_construct     (TYPE)
#define generic_array_list_copy                 array_list_copy          (TYPE)
#define generic_array_list_copy_to              array_list_copy_to       (TYPE)
#define generic_array_list_destroy              array_list_destroy       (TYPE)
#define generic_array_list_push                 array_list_push          (TYPE)
#define generic_array_list_erase                array_list_erase         (TYPE)
#define generic_array_list_at                   array_list_at            (TYPE)
#define generic_array_list_set                  array_list_set           (TYPE)
#define generic_array_list_data                 array_list_data          (TYPE)
#define generic_array_list_const_data           array_list_const_data    (TYPE)
#define generic_array_list_size                 array_list_size          (TYPE)
#define generic_array_list_capacity             array_list_capacity      (TYPE)


//Generic Function Prototypes
static GenericArrayList* generic_array_list_init            (void* addr, Size capacity)                                               ;
static GenericArrayList* generic_array_list_construct       (Size capacity)                                                           ;
static GenericArrayList* generic_array_list_copy            (GenericArrayList const* array)                                           ;
static GenericArrayList* generic_array_list_copy_to         (GenericArrayList const* array, void* dest)                               ;
static void              generic_array_list_destroy         (GenericArrayList      * array)                                           ;
static Bool              generic_array_list_push            (GenericArrayList      * array, TYPE value)                               ;
static Bool              generic_array_list_erase           (GenericArrayList      * array, Index i)                                  ;
static TYPE              generic_array_list_at              (GenericArrayList      * array, Index i)               __attribute_pure__ ;
static void              generic_array_list_set             (GenericArrayList      * array, Index i, TYPE value)                      ;
static TYPE      *       generic_array_list_data            (GenericArrayList      * array)                        __attribute_const__;
static TYPE const*       generic_array_list_const_data      (GenericArrayList const* array)                        __attribute_const__;
static Size              generic_array_list_size            (GenericArrayList const* array)                        __attribute_pure__ ;
static Size              generic_array_list_capacity        (GenericArrayList const* array)                        __attribute_pure__ ;


//Section: Queue
//+===============+
//+               +
//+ GENERIC QUEUE +
//+               +
//+===============+
/**
 * @brief A generic queue data structure
 */
#define Queue(TYPE) TYPED_NAME(Queue, TYPE)
#define GenericQueue Queue(TYPE)

typedef struct {
    Index volatile _tail, _head, _size;
    GenericVector _data;
} GenericQueue;

/**
 * @brief Initializes a block of memory as a generic queue
 *
 * @param addr      The block off memory to initialize
 * @param capacity  The capacity of the queue
 *
 * @return The same address casted as GenericQueue
 */
#define queue_init(TYPE) TYPED_NAME(queue_init, TYPE)

/**
 * @brief Allocates and initializes a block of memory as a generic queue
 *
 * @param capacity  The capacity of the queue
 *
 * @return The adress of the new queue or null if the allocation failed
 */
#define queue_construct(TYPE) TYPED_NAME(queue_construct, TYPE)

/**
 * @brief Allocates enough space and copies into that space the given queue
 *
 * @param queue  The adress of the queue to copy
 *
 * @return The adress of the new queue or null off the allocation fails
 */
#define queue_copy(TYPE) TYPED_NAME(queue_copy, TYPE)

/**
 * @brief Copies an queue to the destination adress
 *
 * @param queue  The adress of the queue to copy
 * @param dest   The destination adress
 *
 * @return The destination adress casted as GenericQueue
 */
#define queue_copy_to(TYPE) TYPED_NAME(queue_copy_to, TYPE)

/**
 * @brief Destroys a queue
 *
 * @param array  The queue to destroy
 */
#define queue_destroy(TYPE) TYPED_NAME(queue_destroy, TYPE)

/**
 * @brief Pushes a value to the queue
 *
 * @param queue  The queue to push into
 * @param value  The value to push
 *
 * @return True if the push was successful, false otherwise
 */
#define queue_push(TYPE) TYPED_NAME(queue_push, TYPE)

/**
 * @brief Gets the head of the queue
 *
 * @param queue  The queue to get the head from
 * @param[OUT]   An adress to place the value into
 *
 * @return True if the queue has any elements, false otherwise
 */
#define queue_front(TYPE) TYPED_NAME(queue_front, TYPE)

/**
 * @brief Pops the head of the queue
 *
 * @param queue  The queue to get the head from
 * @param[OUT]   An adress to place the value into
 *
 * @return True if the queue has any elements, false otherwise
 */
#define queue_pop(TYPE) TYPED_NAME(queue_pop, TYPE)

/**
 * @brief Gets the size of a queue
 *
 * @param queue  The queue to get the size from
 *
 * @return the size of the queue
 */
#define queue_size(TYPE) TYPED_NAME(queue_size, TYPE)

/**
 * @brief Gets the capacity of a queue
 *
 * @param queue  The queue to get the capacity from
 *
 * @return the capacity of the queue
 */
#define queue_capacity(TYPE) TYPED_NAME(queue_capacity, TYPE)

//Generic Name definitions
#define generic_queue_init          queue_init          (TYPE)
#define generic_queue_construct     queue_construct     (TYPE)
#define generic_queue_copy          queue_copy          (TYPE)
#define generic_queue_copy_to       queue_copy_to       (TYPE)
#define generic_queue_destroy       queue_destroy       (TYPE)
#define generic_queue_push          queue_push          (TYPE)
#define generic_queue_front         queue_front         (TYPE)
#define generic_queue_pop           queue_pop           (TYPE)
#define generic_queue_size          queue_size          (TYPE)
#define generic_queue_capacity      queue_capacity      (TYPE)

//Generic Function Prototypes
static GenericQueue* generic_queue_init     (void* addr, Size capacity)                               ;
static GenericQueue* generic_queue_construct(Size capacity)                                           ;
static GenericQueue* generic_queue_copy     (GenericQueue const* queue)                               ;
static GenericQueue* generic_queue_copy_to  (GenericQueue const* queue, void* dest)                   ;
static void          generic_queue_destroy  (GenericQueue      * queue)                               ;
static Bool          generic_queue_push     (GenericQueue      * queue, TYPE value)                   ;
static Bool          generic_queue_front    (GenericQueue      * queue, TYPE* value)                  ;
static Bool          generic_queue_pop      (GenericQueue      * queue, TYPE* value)                  ;
static Size          generic_queue_size     (GenericQueue const* queue)             __attribute_pure__;
static Size          generic_queue_capacity (GenericQueue const* queue)             __attribute_pure__;

//Section: Definitions
//Section: Matrix Definitions
GenericMatrix* generic_matrix_init(void* addr, Size const cols, Size const rows) {
    DEBUG_NULL_CHECK(addr);
    return (GenericMatrix*)memcpy(addr, (void*)&(GenericMatrix){ ._cols=cols, ._rows=rows }, sizeof(GenericMatrix));
}

GenericMatrix* generic_matrix_construct(Size const cols, Size const rows) {
    void* const memory = malloc(sizeof(GenericMatrix) + cols*rows*sizeof(*matrix_const_data(TYPE)(NULL)));
    if_likely(memory != NULL)
        generic_matrix_init(memory, cols, rows);
    return (GenericMatrix*)memory;
}

GenericMatrix* generic_matrix_copy(GenericMatrix const* const mat) {
    DEBUG_NULL_CHECK(mat);
    void* const memory = malloc(sizeof(GenericMatrix) + matrix_size(TYPE)(mat) * sizeof(*matrix_const_data(TYPE)(mat)));
    if_likely(memory != NULL)
        generic_matrix_copy_to(mat, memory);
    return (GenericMatrix*)memory;
}

GenericMatrix* generic_matrix_copy_to(GenericMatrix const* const mat, void* const dest) {
    DEBUG_NULL_CHECK(mat);
    DEBUG_NULL_CHECK(dest);
    return (GenericMatrix*)memcpy(dest, (void*)mat, sizeof(GenericMatrix) + matrix_size(TYPE)(mat) * sizeof(*matrix_const_data(TYPE)(mat)));
}

void generic_matrix_destroy(GenericMatrix* const mat) {
    free(mat);
}

Size generic_matrix_size(GenericMatrix const* const mat) {
    DEBUG_NULL_CHECK(mat);
    return matrix_cols(TYPE)(mat) * matrix_rows(TYPE)(mat);
}

Size generic_matrix_cols(GenericMatrix const* const mat) {
    DEBUG_NULL_CHECK(mat);
    return mat->_cols;
}

Size generic_matrix_rows(GenericMatrix const* const mat) {
    DEBUG_NULL_CHECK(mat);
    return mat->_rows;
}

TYPE const* generic_matrix_const_data(GenericMatrix const* const mat) {
    DEBUG_NULL_CHECK(mat);
    return mat->_data; //Only ptr arithmetics not dereferencing
}

TYPE* generic_matrix_data(GenericMatrix* const mat) {
    DEBUG_NULL_CHECK(mat);
    return mat->_data; //Only ptr arithmetics not dereferencing
}

TYPE generic_matrix_at(GenericMatrix const* const mat, Index const i, Index const j) {
    DEBUG_ASSERT(0 <= j && j < matrix_cols(TYPE)(mat));
    DEBUG_ASSERT(0 <= i && i < matrix_rows(TYPE)(mat));
    return mat->_data[j + i*matrix_cols(TYPE)(mat)];
}

void generic_matrix_set(GenericMatrix* const mat, Index const i, Index const j, TYPE const value)  {
    DEBUG_ASSERT(0 <= j && j < matrix_cols(TYPE)(mat));
    DEBUG_ASSERT(0 <= i && i < matrix_rows(TYPE)(mat));
    mat->_data[j + i*matrix_cols(TYPE)(mat)] = value;
}


//Section: Vector Definitions
GenericVector* generic_vector_init(void* const addr, Size const size) {
    DEBUG_NULL_CHECK(addr);
    return generic_matrix_init(addr, size, 1);
}

GenericVector* generic_vector_construct(Size const size) {
    return generic_matrix_construct(size, 1);
}

GenericVector* generic_vector_copy(GenericVector const* const vec) {
    DEBUG_NULL_CHECK(vec);
    return generic_matrix_copy(vec);
}

GenericVector* generic_vector_copy_to(GenericVector const* const vec, void* const dest) {
    DEBUG_NULL_CHECK(vec);
    DEBUG_NULL_CHECK(dest);
    return generic_matrix_copy_to(vec, dest);
}

void generic_vector_destroy(GenericVector* const vec) {
    generic_matrix_destroy(vec);
}

Size generic_vector_size(GenericVector const* const vec) {
    DEBUG_NULL_CHECK(vec);
    return generic_matrix_size(vec);
}

TYPE* generic_vector_data(GenericVector* const vec) {
    DEBUG_NULL_CHECK(vec);
    return generic_matrix_data(vec);
}

TYPE const* generic_vector_const_data(GenericVector const* const vec) {
    DEBUG_NULL_CHECK(vec);
    return generic_matrix_const_data(vec);
}

TYPE generic_vector_at(GenericVector const* const vec, Index const i) {
    DEBUG_NULL_CHECK(vec);
    DEBUG_ASSERT(0 <= i && i < generic_vector_size(vec));
    return generic_matrix_at(vec, 0, i);
}

void generic_vector_set(GenericVector* const vec, Index const i, TYPE value) {
    DEBUG_NULL_CHECK(vec);
    DEBUG_ASSERT(0 <= i && i < generic_vector_size(vec));
    generic_matrix_set(vec, 0, i, value);
}


//Section: ArrayList Definitions
GenericArrayList* generic_array_list_init(void* const addr, Size const capacity) {
    DEBUG_NULL_CHECK(addr);
    memcpy(addr, (void*)&(GenericArrayList){ ._size=0 }, sizeof(GenericArrayList));
    generic_vector_init(&((GenericArrayList*)addr)->_data, capacity);
    return (GenericArrayList*)addr;
}

GenericArrayList* generic_array_list_construct(Size const capacity) {
    void* const memory = malloc(sizeof(GenericArrayList) + capacity*sizeof(*((GenericArrayList*)NULL)->_data._data));
    if_likely(memory != NULL)
        generic_array_list_init(memory, capacity);
    return (GenericArrayList*)memory;
}

GenericArrayList* generic_array_list_copy(GenericArrayList const* const array) {
    DEBUG_NULL_CHECK(array);
    void* const memory = malloc(sizeof(GenericArrayList) + generic_array_list_capacity(array)*sizeof(*((GenericArrayList*)NULL)->_data._data));
    if_likely(memory != NULL)
        generic_array_list_copy_to(array, memory);
    return (GenericArrayList*)memory;
}

GenericArrayList* generic_array_list_copy_to(GenericArrayList const* array, void* dest) {
    DEBUG_NULL_CHECK(array);
    DEBUG_NULL_CHECK(dest);
    return (GenericArrayList*)memcpy(dest, (void*)array, sizeof(GenericArrayList) + generic_array_list_capacity(array)*sizeof(*((GenericArrayList*)NULL)->_data._data));
}

void generic_array_list_destroy(GenericArrayList* const array) {
    free(array);
}

Bool generic_array_list_push(GenericArrayList* const array, TYPE const value) {
    DEBUG_NULL_CHECK(array);
    if(generic_array_list_size(array) == generic_array_list_capacity(array))
        return false;
    vector_set(TYPE)(&array->_data, array->_size++, value);
    return true;
}

Bool generic_array_list_erase(GenericArrayList* const array, Index const i) {
    DEBUG_NULL_CHECK(array);
    if(i >= generic_array_list_size(array))
        return false;
    memmove((void*)&generic_vector_data(&array->_data)[i], (void*)&generic_vector_data(&array->_data)[i+1], (generic_array_list_size(array)-i-1)*sizeof(*generic_vector_data(&array->_data)));
    array->_size--;
    return true;
}

TYPE generic_array_list_at(GenericArrayList* const array, Index const i) {
    DEBUG_NULL_CHECK(array);
    DEBUG_ASSERT(0 <= i && i < generic_array_list_size(array));
    return vector_at(TYPE)(&array->_data, i);
}

void generic_array_list_set(GenericArrayList* const array, Index const i, TYPE const value) {
    DEBUG_NULL_CHECK(array);
    DEBUG_ASSERT(0 <= i && i < generic_array_list_size(array));
    generic_vector_set(&array->_data, i, value);
}

TYPE* generic_array_list_data(GenericArrayList* array) {
    DEBUG_NULL_CHECK(array);
    return generic_vector_data(&array->_data);
}

TYPE const* generic_array_list_const_data(GenericArrayList const* array) {
    DEBUG_NULL_CHECK(array);
    return generic_vector_const_data(&array->_data);
}

Size generic_array_list_size(GenericArrayList const* const array) {
    DEBUG_NULL_CHECK(array);
    return array->_size;
}

Size generic_array_list_capacity(GenericArrayList const* const array) {
    DEBUG_NULL_CHECK(array);
    return generic_vector_size(&array->_data);
}



//Section: Queue Definitions
GenericQueue* generic_queue_init(void* const addr, Size const capacity) {
    DEBUG_NULL_CHECK(addr);
    memcpy(addr, (void*)&(GenericQueue){ ._tail=0, ._head=0, ._size=0 }, sizeof(GenericQueue));
    generic_vector_init(&((GenericQueue*)addr)->_data, capacity);
    return (GenericQueue*)addr;
}

GenericQueue* generic_queue_construct(Size const capacity) {
    void* const memory = malloc(sizeof(GenericQueue) + capacity*sizeof(*((GenericQueue*)NULL)->_data._data));
    if_likely(memory != NULL)
        generic_queue_init(memory, capacity);
    return (GenericQueue*)memory;
}

GenericQueue* generic_queue_copy(GenericQueue const* const queue) {
    DEBUG_NULL_CHECK(queue);
    void* const memory = malloc(sizeof(GenericQueue) + generic_queue_capacity(queue)*sizeof(*((GenericQueue*)NULL)->_data._data));
    if_likely(memory != NULL)
        generic_queue_copy_to(queue, memory);
    return (GenericQueue*)memory;
}

GenericQueue* generic_queue_copy_to(GenericQueue const* queue, void* dest) {
    DEBUG_NULL_CHECK(queue);
    DEBUG_NULL_CHECK(dest);
    return (GenericQueue*)memcpy(dest, (void*)queue, sizeof(GenericQueue) + generic_queue_capacity(queue)*sizeof(*((GenericQueue*)NULL)->_data._data));
}

void generic_queue_destroy(GenericQueue* const queue) {
    free(queue);
}

Bool generic_queue_push(GenericQueue* const queue, TYPE const value) {
    DEBUG_NULL_CHECK(queue);
    if(generic_queue_size(queue) == generic_queue_capacity(queue))
        return false;
    generic_vector_set(&queue->_data, queue->_tail++, value);
    if_unlikely(queue->_tail == generic_vector_size(&queue->_data))
        queue->_tail = 0;
    queue->_size++;
    return true;
}

Bool generic_queue_front(GenericQueue* const queue, TYPE* const value) {
    DEBUG_NULL_CHECK(queue);
    if(generic_queue_size(queue) == 0)
        return false;
    if(value != NULL)
        *value = generic_vector_at(&queue->_data, queue->_head);
    return true;
}

Bool generic_queue_pop(GenericQueue* const queue, TYPE* const value) {
    DEBUG_NULL_CHECK(queue);
    if(generic_queue_size(queue) == 0)
        return false;
    if(!generic_queue_front(queue, value))
        return false;
    if_unlikely(++queue->_head == generic_vector_size(&queue->_data))
        queue->_head = 0;
    queue->_size--;
    return true;
}

Size generic_queue_size(GenericQueue const* const queue) {
    DEBUG_NULL_CHECK(queue);
    return queue->_size;
}

Size generic_queue_capacity(GenericQueue const* const queue) {
    DEBUG_NULL_CHECK(queue);
    return generic_vector_size(&queue->_data);
}

//Section: Algorithms

/**
 * @brief Sorts an array
 *
 * @param data        The begining of the array
 * @param size        The size of the array
 * @param compare     A Function to compare the values
 */
#define sort(TYPE)                  TYPED_NAME(sort,           TYPE)

/**
 * @brief Executes a search on a dataset, return the offset of the requested element or -1 if no element found
 * 
 * @param data       The data to search
 * @param size       The size of the data
 * @param value      The target value
 * @param compare    A Function to compare the values
 * 
 * @return The index to the selected element, or -1
 */
#define binary_search(TYPE) TYPED_NAME(binary_search, TYPE)

/**
 * @brief Reduces an array to an element
 *
 * @param data        The begining of the array
 * @param size        The size of the array
 * @param op          A Function applied to all elements to produce the reduction
 * @param out[OUT]    An out parameter to store the result
 */
#define reduce(TYPE)                TYPED_NAME(reduce,         TYPE)

/**
 * @brief Picks an element given a predicate (Type of reduction)
 *
 * @param data        The begining of the array
 * @param size        The size of the array
 * @param predicate   A Function which denotes if an item of the array is to be selected
 *
 * @return The adress of the selected item, null otherwise
 */
#define pick(TYPE)                  TYPED_NAME(pick,           TYPE)

#define Comparator(TYPE)            TYPED_NAME(Comparator,     TYPE)
#define BinaryOperator(TYPE)        TYPED_NAME(BinaryOperator, TYPE)
#define Predicate(TYPE)             TYPED_NAME(Predicate,      TYPE)

#define generic_sort          sort                  (TYPE)
#define generic_binary_search binary_search         (TYPE)
#define generic_reduce        reduce                (TYPE)
#define generic_pick          pick                  (TYPE)
#define GenericComparator     Comparator            (TYPE)
#define GenericBinaryOperator BinaryOperator        (TYPE)
#define GenericPredicate      Predicate             (TYPE)

typedef Bool    (*GenericComparator)            (TYPE const*, TYPE const*);
typedef void    (*GenericBinaryOperator)        (TYPE      *, TYPE const*);
typedef TYPE*   (*GenericPredicate)             (TYPE const*);

static void           generic_sort      (TYPE      * data, Size size, GenericComparator comparator);
static void           generic_reduce    (TYPE const* data, Size size, GenericBinaryOperator op, TYPE* out);
static TYPE const*    generic_pick      (TYPE const* data, Size size, GenericPredicate predicate);

//Section: Algorithms Definitions
void generic_sort(TYPE* const data, Size const size, GenericComparator const compare) {
    //TODO: Test values for THRESHOLD
    #define SIZE_THRESHOLD 100
    #pragma omp single nowait
    {
        DEBUG_NULL_CHECK(data);

        if_likely(size > 1) {
            Index       m     = size/2;
            Size  const ls    = m;
            Size  const rs    = size - ls;
            TYPE* const left  = data;
            TYPE* const right = data+ls;

            #pragma omp task if(ls > SIZE_THRESHOLD)
            generic_sort(left,  ls, compare);

            #pragma omp task if(rs > SIZE_THRESHOLD)
            generic_sort(right, rs, compare);

            #pragma omp taskwait

            if( compare(&left[ls-1], &right[0]) > 0) {
                Index l = 0, r = 0;
                while( l < m && r < rs ) {
                    if( compare(&left[l], &right[r]) <= 0 ) {
                        l++;
                    } else {
                        TYPE const value = right[r];
                        memmove((void*)&left[l+1], (void*)&left[l], (ls + r - l) * sizeof(TYPE)); //Shift all one to the right, covering right[r]
                        left[l] = value;
                        l++;
                        m++;
                        r++;
                    }
                }
            }
        }
    }

    #undef SIZE_THRESHOLD
}


static Index generic_binary_search(TYPE const* const data, Size const size, TYPE const* const value, GenericComparator compare) {
    DEBUG_NULL_CHECK(   data  );
    DEBUG_NULL_CHECK( compare );

    if( size <= 0)
        return -1;

    Index const m = size/2;
    Bool  const c = compare(&data[m], value);

    if( c == 0 )
        return m;

    if( c > 0 )
        return generic_binary_search(data, m, value, compare);

    return generic_binary_search(&data[m+1], size-(m+1), value, compare);
}


void generic_reduce(TYPE const* const data, Size const size, GenericBinaryOperator const op, TYPE* const out) {
    DEBUG_NULL_CHECK(data);
    DEBUG_NULL_CHECK(out);

    for(Index i=0; i < size; ++i)
        op(out, &data[i]);
}

TYPE const* generic_pick(TYPE const* const data, Size const size, GenericPredicate const predicate) {
    DEBUG_NULL_CHECK(data);

    for(Index i = 0; i < size; ++i)
        if(predicate(&data[i]))
            return &data[i];

    return NULL;
}


//Section: Undefs
#undef generic_sort
#undef generic_binary_search
#undef generic_reduce
#undef generic_pick
#undef GenericComparator
#undef GenericBinaryOperator
#undef GenericPredicate

#undef GenericQueue
#undef generic_queue_init
#undef generic_queue_construct
#undef generic_queue_destroy
#undef generic_queue_push
#undef generic_queue_pop
#undef generic_queue_size

#undef GenericArrayList
#undef generic_array_list_init
#undef generic_array_list_construct
#undef generic_array_list_copy
#undef generic_array_list_copy_to
#undef generic_array_list_destroy
#undef generic_array_list_push
#undef generic_array_list_at
#undef generic_array_list_set
#undef generic_array_list_size
#undef generic_array_list_capacity

#undef GenericVector
#undef generic_vector_init
#undef generic_vector_copy
#undef generic_vector_destroy
#undef generic_vector_size
#undef generic_vector_data
#undef generic_vector_set

#undef GenericMatrix
#undef generic_matrix_init
#undef generic_matrix_destroy
#undef generic_matrix_size
#undef generic_matrix_cols
#undef generic_matrix_rows
#undef generic_matrix_const_data
#undef generic_matrix_data
#undef generic_matrix_at
#undef generic_matrix_set
#undef generic_matrix_copy

#endif

