#pragma once

#include <stddef.h>

typedef long          Index;
typedef long          Size;
typedef char          Bool;
typedef Bool          Byte;
typedef char const*   String;

#define true  ((Bool)!0)
#define false ((Bool) 0)

#ifdef __GNUC__
#define likely(EXPR)       __builtin_expect((EXPR), true)
#define unlikely(EXPR)     __builtin_expect((EXPR), false)
#else
#define likely(EXPR)       (EXPR)
#define unlikely(EXPR)     (EXPR)
#endif

#define if_likely(EXPR)   if(likely(EXPR))
#define if_unlikely(EXPR) if(unlikely(EXPR))


#ifndef NDEBUG
#include <signal.h>
#define BREAK_POINT raise(SIGTRAP)
#else
#define BREAK_POINT ((void)0)
#endif


#define ASSERT(EXPR)            assert_(EXPR, #EXPR, __FILE__, __LINE__)
#define NULL_CHECK(EXPR)        ASSERT((EXPR) != NULL)
#ifndef NDEBUG
#define DEBUG_NULL_CHECK(EXPR)  NULL_CHECK(EXPR)
#define DEBUG_ASSERT(EXPR)      ASSERT(EXPR)
#else
#define DEBUG_ASSERT(EXPR)      (EXPR)
#define DEBUG_NULL_CHECK(EXPR)  (EXPR)
#endif
void assert_(Bool expression, String code, String file, Size line);
