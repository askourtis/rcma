#include <omp.h>
#include "cm.h"
#include <memory.h>


typedef Bool volatile BoolType;

#define TYPE BoolType
#include "generics.h"
#undef TYPE


/**
 * @brief Global Degrees (For use in compare_degrees and find_min_degree_index)
 */
static Vector(DataType)* GD = NULL;

/**
 * @brief A comparator (For use in sort)
 */
static Bool compare_degrees(IndexType const* i, IndexType const* j) __attribute_pure__;

/**
 * @brief A comparator (For use in binary_search)
 */
static Bool compare_indicies(IndexType const* i, IndexType const* j) __attribute_pure__;

/**
 * @brief Selects The minimum degree element if it is allowed by the NotVisited Vector and returns its index
 *
 * @param V  The Validity Vector
 *
 * @return An index to the element or -1 if the NotVisited Vector didnt allow any of the elements or the size was 0
 */
static Index find_min_degree_index(Vector(BoolType) const* V) __attribute_pure__;

/**
 * @brief Calculates the degree of each row or column of a sparse symetric matrix
 *
 * @param I         The row indicies
 * @param J         The column indicies
 * @param E         The entries of the sparse matrix
 * @param L         The total number of entries
 * @param D[OUT]    An N sized vector to store the degrees of each row
 * @param V[OUT]    AN N sized vector to store a flag for each valid degree
 */
static void cms_degree(IndexType const* I, IndexType const* J, DataType const* E, Size L, Vector(DataType)* D, Vector(BoolType)* V);

/**
 * @brief Finds the begining and the end of a region with constant value in a sorted array
 * 
 * @param arr        The array
 * @param val        The target value
 * @param count      The size of the array
 * @param begin[OUT] The begin of the region
 * @param end[OUT]   The end of the region
 */
static void find_region_of(IndexType const* arr, Index val, Size count, Index* begin, Index* end);

/**
 * @brief Using binary search, finds the first element greater than the given value
 * 
 * @param arr   The array to search into
 * @param val   The value to compare with
 * @param count The size of the array
 * 
 * @return The index to the first greater element or -1 if there is none
 */
static Index next_greater(IndexType const* arr, Index val, Size count) __attribute_pure__;

//Definitions
ArrayList(IndexType)* cms(IndexType const* const I,
                          IndexType const* const J,
                          DataType  const* const E,
                          SizeType         const N,
                          SizeType         const L)
{
    DEBUG_NULL_CHECK( I );
    DEBUG_NULL_CHECK( J );
    DEBUG_NULL_CHECK( E );
    DEBUG_ASSERT(  N >= 0 );
    DEBUG_ASSERT(  L >= 0 );
    DEBUG_ASSERT( N*N >= L);

    //Needed data structures
    static SizeType        vs = -1;
    Vector(BoolType)     * V  = NULL;
    Vector(DataType)     * D  = NULL;
    Queue(IndexType)     * Q  = NULL;
    ArrayList(IndexType) * R  = NULL;
    ArrayList(IndexType) * B  = NULL;

    #pragma omp single copyprivate(V, D, Q, R, B)
    {
        //Construct data structures
        DEBUG_NULL_CHECK( V = vector_construct     (BoolType ) ( N ) );
        DEBUG_NULL_CHECK( D = vector_construct     (DataType ) ( N ) );
        DEBUG_NULL_CHECK( Q = queue_construct      (IndexType) ( N ) );
        DEBUG_NULL_CHECK( R = array_list_construct (IndexType) ( N ) );
        DEBUG_NULL_CHECK( B = array_list_construct (IndexType) ( N ) );
    }

    DEBUG_NULL_CHECK( D );
    DEBUG_NULL_CHECK( V );
    DEBUG_NULL_CHECK( Q );
    DEBUG_NULL_CHECK( R );
    DEBUG_NULL_CHECK( B );

    //Calculate Degrees
    cms_degree(I, J, E, L, GD = D, V);

    //Find the number of trues in the V vector
    #pragma omp for reduction(+ : vs)
    for(Index i=0; i<N; ++i)
        vs += vector_at(BoolType)(V, i);

    //While there are valid nodes
    while(vs > 0) {
        //Index of minimum degree
        Index const mi = find_min_degree_index(V);

        DEBUG_ASSERT( 0 <= mi && mi < N );

        #pragma omp single
        {
            DEBUG_ASSERT( queue_push(IndexType)(Q, mi) );

            //Consider the minimum degree as visited (=> Set NV[mi] = false and decrease vs)
            vector_set(BoolType)(V, mi, false);
            vs--;
        }

        
        do {
            Index j  = -1;

            //Initialize variables
            #pragma omp single copyprivate(j)
            {
                DEBUG_NULL_CHECK( B = array_list_init(IndexType)(B, N) );
                DEBUG_ASSERT    (    queue_front(IndexType)(Q, &j)     );
                DEBUG_ASSERT    (            0 <= j && j < N           );
                DEBUG_ASSERT    (   array_list_push(IndexType)(R, j)   );
            }

            {
                Index begin=-1, end=-1;
                //Find a consecutive region where J == j
                #pragma omp single copyprivate(begin, end)
                {
                    find_region_of(J, j, L, &begin, &end);
                }

                //Loop over this region and select all row indicies since they are considered neighbours
                #pragma omp for schedule(auto)
                for(Index idx=begin; idx<=end; ++idx) {
                    Index const i = I[idx];

                    if_unlikely( i == j )   //[Unlikly that the element will be on a diagonal]
                        continue;
                    
                    if( vector_at(BoolType)(V, i) == false )
                        continue;

                    vector_set(BoolType)(V, i, false);

                    #pragma omp critical
                    {
                        //Assume found neighbour as visited
                        DEBUG_ASSERT( array_list_push(IndexType)(B, i) );
                        vs--;
                    }
                }

                #pragma omp for schedule(auto)
                for(Index idx=0; idx<begin; ++idx) {
                    //Select all elements with row index same as the selected column index
                    if_likely( I[idx] != j ) //[Likely that the indicies are not the same]
                        continue;

                    //Since the matrix is symetrix both (i, j) and (j, i) are part of the matrix
                    Index const i = J[idx];

                    if_unlikely( i == j ) //[Unlikly that the element will be on a diagonal]
                        continue;
                    
                    if( vector_at(BoolType)(V, i) == false )
                        continue;

                    vector_set(BoolType)(V, i, false);

                    #pragma omp critical
                    {
                        DEBUG_ASSERT( array_list_push(IndexType)(B, i) );
                        vs--;
                    }
                }
                
                #pragma omp for schedule(auto)
                for(Index idx=end+1; idx<L; ++idx) {
                    if_likely( I[idx] != j )
                        continue;
                    Index const i = J[idx];

                    if_unlikely( i == j )
                        continue;
                    
                    if( vector_at(BoolType)(V, i) == false )
                        continue;

                    vector_set(BoolType)(V, i, false);

                    #pragma omp critical
                    {
                        DEBUG_ASSERT( array_list_push(IndexType)(B, i) );
                        vs--;
                    }
                }
            }

            //Sort found indicies
            sort(IndexType)(array_list_data(IndexType)(B), array_list_size(IndexType)(B), compare_degrees);
            #pragma omp barrier

            #pragma omp single
            {
                //Pop the front element of the queue since we used it
                DEBUG_ASSERT( queue_pop(IndexType)(Q, NULL) );
                //Push all found indicies to the queue
                //TODO: Push all
                for(Index i=0; i < array_list_size(IndexType)(B); ++i)
                    DEBUG_ASSERT( queue_push(IndexType)(Q, array_list_at(IndexType)(B, i)) );
            }
        } while(queue_size(IndexType)(Q));
    }

    #pragma omp barrier
    #pragma omp single nowait
    {
        //Free allocated memory
        vector_destroy      (DataType)  (D);
        queue_destroy       (IndexType) (Q);
        vector_destroy      (BoolType)  (V);
        array_list_destroy  (IndexType) (B);

        GD = NULL;
    }

    return R;

}


ArrayList(IndexType)* rcms(IndexType const* const I,
                           IndexType const* const J,
                           DataType  const* const V,
                           SizeType         const N,
                           SizeType         const L)
{
    DEBUG_NULL_CHECK( I );
    DEBUG_NULL_CHECK( J );
    DEBUG_NULL_CHECK( V );
    DEBUG_ASSERT(  N >= 0 );
    DEBUG_ASSERT(  L >= 0 );
    DEBUG_ASSERT( N*N >= L);

    ArrayList(IndexType)* const permutation = cms(I, J, V, N, L);

    DEBUG_NULL_CHECK(permutation);

    Index      const size = array_list_size(IndexType)(permutation);
    IndexType* const data = array_list_data(IndexType)(permutation);

    #define SWAP(X, Y) do{ DataType Z = X; X = Y; Y = Z; } while(0)

    //Reverse
    #pragma omp for schedule(auto)
        for(Index i = 0; i < size/2; ++i)
            SWAP( data[i], data[size-i-1] );

    #undef SWAP

    return permutation;
}



//Helper Definitions
Index next_greater(IndexType const* const arr, Index const val, Size const count) {
    DEBUG_NULL_CHECK( arr );

    Index begin =  0;       //Point to the begining of the array
    Index end   = count-1;  //Point to the end
    Index ret   = -1;       //Default initialize return

    //While the pointers didnt swap
    while( begin <= end ) {
        //Point to the middle of begin and end
        Index const m = (end + begin)/2;

        //If the value is greater or equal of the selected element
        if (arr[m] <= val) {
            //Set the right element of the middle pointer as the new begin
            begin = m+1;
        } else {
            //Found at least one greater element (set return)
            //Assume the end is the element before that
            ret = m;
            end = m-1;
        }
    }

    return ret;
} 

void find_region_of(IndexType const* const arr, Index const val, Size const count, Index* const begin, Index* const end) {
    DEBUG_NULL_CHECK(  arr  );
    DEBUG_NULL_CHECK( begin );
    DEBUG_NULL_CHECK(  end  );

    //Find the begin of the val region
    *begin = next_greater(arr, val-1, count);

    //If there is no value
    if( *begin < 0 || arr[*begin] != val ) {
        *end = *begin-1;
        return;
    }

    //Find the begin of the next region
    //Dont search the whole array since we know that the end is greater than the begin
    *end = next_greater(&arr[*begin+1], val, count-(*begin+1));

    //If no end then set the end to be the last element of the array
    if( *end < 0 ) {
        *end = count-1;
        return;
    }

    //Set the end with the propper offset
    *end += *begin + 1;
    return;
}

void cms_degree(IndexType  const* const I,
                IndexType  const* const J,
                DataType   const* const E,
                Size              const L,
                Vector(DataType)* const D,
                Vector(BoolType)* const V)
{
    DEBUG_NULL_CHECK( I );
    DEBUG_NULL_CHECK( E );
    DEBUG_ASSERT( L >= 0 );
    DEBUG_NULL_CHECK( D );
    DEBUG_NULL_CHECK( V );
    DEBUG_ASSERT( vector_size(DataType)(D) == vector_size(BoolType)(V) );

    Size const N = vector_size(DataType)(D);

    DataType* const raw_d = vector_data(DataType)(D);
    BoolType* const raw_v = vector_data(BoolType)(V);

    DEBUG_NULL_CHECK( raw_d );
    DEBUG_NULL_CHECK( raw_v );

    #pragma omp single
    {
        //Initialize raw to zeros
        memset((void*)raw_d, 0,     N*sizeof(IndexType));
        memset((void*)raw_v, false, N*sizeof(BoolType ));
    }

    //I array is not sorted thus not good chacing
    //For each (i, e) in (I, E) -> degrees[i] += e And valid[i] = true
    #pragma omp for schedule(auto)
    for(Index idx = 0; idx < L; ++idx) {
        raw_v[ I[idx] ] = true;

        #pragma omp atomic
        raw_d[ I[idx] ] += E[idx];
    }

    //For each (j, e) in (J, E) -> if not on diagonal: degrees[j] += e And valid[j] = true
    #pragma omp for schedule(auto)
    for(Index idx = 0; idx < L; ++idx) {
        if_unlikely( J[idx] == I[idx] )
            continue;

        raw_v[ J[idx] ] = true;

        #pragma omp atomic
        raw_d[ J[idx] ] += E[idx];
    }

}


Bool compare_degrees(IndexType const* const i, IndexType const* const j) {
    DEBUG_NULL_CHECK( GD );
    DEBUG_NULL_CHECK( i  );
    DEBUG_NULL_CHECK( j  );

    DataType const first  = vector_at(DataType)(GD, *i);
    DataType const second = vector_at(DataType)(GD, *j);

    if( first == second )
        return 0;
    if( first > second )
        return 1;
    return -1;
}

Bool compare_indicies(IndexType const* const i, IndexType const* const j) {
    DEBUG_NULL_CHECK( i );
    DEBUG_NULL_CHECK( j );

    if( *i == *j )
        return 0;
    if( *i > *j  )
        return 1;
    return -1;
}


Index find_min_degree_index(Vector(BoolType) const* const V) {
    DEBUG_NULL_CHECK( V );
    DEBUG_NULL_CHECK( GD );

    //Reduction variable
    static IndexType min;

    //Local variable
    Index local = -1;

    //Initialize the reduction variable to -1
    #pragma omp single
        min = -1;

    //Calculate local minimum
    #pragma omp for schedule(auto) nowait
        for(Index i = 0; i < vector_size(BoolType)(V); ++i)
            if( vector_at(BoolType)(V, i) )
                if( local < 0 || compare_degrees(&i, &local) <= 0 )
                    local = i;

    //Reduce to global reduction variable, if the local variable has a legal value
    if(local >= 0)
    {
        #pragma omp critical
        {
            if(min < 0 || compare_degrees(&local, &min) <= 0)
                min = local;
        }
    }
    #pragma omp barrier

    return min;
}
