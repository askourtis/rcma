#include <definitions.h>
#include <stdlib.h>
#include <stdio.h>

void assert_(Bool const expression, String const code, String const file, Size const line) {
    if_unlikely(!expression) {
        fprintf(stderr, "Assertion Failed:\n  %s:%ld\t%s\n", file, line, code);
        BREAK_POINT;
        exit(1);
    }
}