# Implementing The Reverse Cuthill-McKee Algorithm in parallel using OpenMP

An iterative version of the CM algorithm was implemented using OpenMP

## The Cuthill-McKee Algorithm
The CM algorithm computes the permutation of a symmetric sparse matrix to reduce its bandwidth

## Code Examples
```c
#include "cm.h"

int main( void )
{
    IndexType* I, J; //The row and column indices
    DataType * E;    //The entries
    SizeType   N;    //The dimension size
    SizeType   L;    //The number of entries

    //LOAD DATA TO I, J, E, N, L

    ArrayList(IndexType)* permutations; //Results

    //Sequential Execution
    permutations = rcms(I, J, E, N, L);

    //Parallel Execution
    #pragma omp parallel
    {
        permutations = rcms(I, J, E, N, L);
    }
}
```

## Sequential Vs Parallel usage
Both implementations are equivalent, but for small matricies sequential execution produce same execution times as the parallel one, if not better.

Also if using sequential implementantion, make sure to change the definitions of `Sizetype`, `IndexType`, `DataType` to non-volatile versions in the [cm.h](./inc/cm.h) file


## Compilation
Place a new source file in the [src](./src) folder and implement the main function.

Execute:
```
$ make
```
### DEBUG Mode vs Optimized Mode
To change to non-debug mode change the [makefile](./makefile)

From:
```makefile
CFLAGS := -I$(INC_DIR) -ggdb3 -fopenmp
```

To:
```makefile
CFLAGS := -I$(INC_DIR) -O3 -D NDEBUG -fopenmp
```