
CC := gcc

SRC_DIR := ./src
INC_DIR := ./inc
OBJ_DIR := ./obj
BLD_DIR := ./build

EXECUTABLE := $(BLD_DIR)/runnable.a


CFLAGS := -I$(INC_DIR) -ggdb3 -fopenmp
LFLAGS := -fopenmp


SOURCES := $(wildcard $(SRC_DIR)/*.c)
OBJECTS := $(patsubst $(SRC_DIR)/%.c, $(OBJ_DIR)/%.o, $(SOURCES))



all: purge_compile

clean_compile: clean compile
purge_compile: purge compile

compile: $(OBJECTS)
	$(CC) $(CFLAGS) $^ -o $(EXECUTABLE) $(LFLAGS)

#Compile each
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c mkdirs
	$(CC) $(CFLAGS) -c $< -o $@

.PHONY: mkdirs
mkdirs:
	mkdir -p $(OBJ_DIR)
	mkdir -p $(BLD_DIR)

.PHONY: clean
clean:
	rm -rf $(OBJ_DIR)/*

.PHONY: purge
purge: clean
	rm -rf $(BLD_DIR)/*